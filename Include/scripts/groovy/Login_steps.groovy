import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Login_steps {

	@Given("get into ecommerce quba")
	def navigatetobrowser(){
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://ecbank.techgentsia.com/app/#!')
	}

	@When("user id and password is there")
	def navigatetouseridpass(){
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Ecommerce Login/Page_Ecommerce Login/div_User ID'), 4)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Ecommerce Login/Page_Ecommerce Login/div_Password'), 4)
	}

	@Then("enter user id")
	def navigatetouserid(){
		WebUI.setText(findTestObject('Object Repository/Page_Ecommerce Login/input_User ID_v-textfield v-widget c-login-_994922'),'tstuser')
	}

	@And("enter password")
	def navigatetopassword(){
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Ecommerce Login/input_Password_v-textfield v-widget c-login_2d08c2'),'8SQVv/p9jVScEs4/2CZsLw==')
	}

	@And("click login")
	def navigatetologin(){
		WebUI.click(findTestObject('Object Repository/Page_Ecommerce Login/div_Login'))
	}

	@And("check product navbar")
	def navigateproductnav(){
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Ecommerce Login/Page_Ecommerce/span_Product'), 5)
	}
}