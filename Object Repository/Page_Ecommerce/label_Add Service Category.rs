<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Add Service Category</name>
   <tag></tag>
   <elementGuidId>7413479b-fa1e-4945-a56b-4375ba3b75b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app-96801']/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div/span/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>gwt-uid-5</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Service Category</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app-96801&quot;)/div[@class=&quot;v-ui v-scrollable&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget c-window-layout v-verticallayout-c-window-layout v-has-width v-has-height&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot v-slot-c-sidemenu-layout&quot;]/div[@class=&quot;v-horizontallayout v-layout v-horizontal v-widget c-sidemenu-layout v-horizontallayout-c-sidemenu-layout v-has-width v-has-height&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot v-slot-c-app-workarea-tabbed v-slot-c-app-workarea-windows&quot;]/div[@class=&quot;c-app-workarea v-layout v-widget v-has-width v-has-height c-app-workarea-tabbed c-app-workarea-c-app-workarea-tabbed c-app-workarea-windows c-app-workarea-c-app-workarea-windows&quot;]/div[@class=&quot;v-tabsheet v-widget v-has-width v-has-height c-main-tabsheet v-tabsheet-c-main-tabsheet framed v-tabsheet-framed padded-tabbar v-tabsheet-padded-tabbar&quot;]/div[@class=&quot;v-tabsheet-content v-tabsheet-content-c-main-tabsheet v-tabsheet-content-framed v-tabsheet-content-padded-tabbar&quot;]/div[@class=&quot;v-tabsheet-tabsheetpanel&quot;]/div[@class=&quot;v-scrollable&quot;]/div[@class=&quot;c-app-window-wrap v-layout v-widget v-has-width v-has-height c-app-tabbed-window c-app-window-wrap-c-app-tabbed-window c-breadcrumbs-visible c-app-window-wrap-c-breadcrumbs-visible&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget c-window-layout v-verticallayout-c-window-layout v-has-width v-has-height&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-select-optiongroup v-widget&quot;]/span[@class=&quot;v-checkbox v-select-option&quot;]/label[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app-96801']/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div/span/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ProductCategory editor'])[2]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Product Category'])[1]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ctrl+\'])[1]/preceding::label[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Service Category']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
   </webElementXpaths>
</WebElementEntity>
