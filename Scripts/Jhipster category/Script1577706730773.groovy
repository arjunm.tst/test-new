import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/a_Category'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/span_Category Name'), 1)
WebUI.click(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/Page_ecommerceadmin/button_Create a new Category'))

WebUI.selectAllOption(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/select_Root Level'))

WebUI.uploadFile(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/Page_ecommerceadmin/Page_ecommerceadmin/input_Category Image_categoryImage'),'Users/tst_mac10/Desktop/books.jpg')

WebUI.setText(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/input_Category Name_categoryName'),'ghrhjrshe')	

WebUI.click(findTestObject('Object Repository/Jhipster_Category/Page_ecommerceadmin/button_Save'))
	
